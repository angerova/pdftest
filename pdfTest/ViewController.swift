
import UIKit
import PDFKit

class ViewController: UIViewController {
    
    @IBOutlet weak var pdfView: PDFView!
    

 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filePath = (documentsDirectory as NSString).appendingPathComponent("foo.pdf") as String
        
        let pdfTitle = "Alina PDF"
        let pdfText = "Some text"
        let pdfMetadata = [
            // The name of the application creating the PDF.
            kCGPDFContextCreator: "Your iOS App",
            
            // The name of the PDF's author.
            kCGPDFContextAuthor: "Foo Bar",
            
            // The title of the PDF.
            kCGPDFContextTitle: "Lorem Ipsum",
            
            // Encrypts the document with the value as the owner password. Used to enable/disable different permissions.
            kCGPDFContextOwnerPassword: "myPassword123"
        ]
        
        // Creates a new PDF file at the specified path.
        UIGraphicsBeginPDFContextToFile(filePath, CGRect.zero, pdfMetadata)
        
        // Creates a new page in the current PDF context.
        UIGraphicsBeginPDFPage()
        
        // Default size of the page is 612x72.
        let pageSize = UIGraphicsGetPDFContextBounds().size
       
        let fontTitle = UIFont.preferredFont(forTextStyle: .headline)
        let fontText = UIFont.preferredFont(forTextStyle: .callout)
        
        // Let's draw the title of the PDF on top of the page.
        let attributedPDFTitle = NSAttributedString(string: pdfTitle, attributes: [NSAttributedString.Key.font: fontTitle])
        let stringSize = attributedPDFTitle.size()
        let stringRect = CGRect(x: (pageSize.width / 2 - stringSize.width / 2), y: 20, width: stringSize.width, height: stringSize.height)
        attributedPDFTitle.draw(in: stringRect)
        
        
        let attributedPDFText = NSAttributedString(string: pdfText, attributes: [NSAttributedString.Key.font: fontText])
        let stringTextSize = attributedPDFText.size()
        let stringTextRect = CGRect(x: 100, y: 200, width: stringTextSize.width, height: stringTextSize.height)
        attributedPDFText.draw(in: stringTextRect)
        
        // Closes the current PDF context and ends writing to the file.
        UIGraphicsEndPDFContext()
        
        
        
        // Create a PDFDocument object and set it as PDFView's document to load the document in that view.
        let pdfDocument = PDFDocument(url: URL(fileURLWithPath: filePath))!
        pdfView.document = pdfDocument
    
       
        let squareAnnotation = PDFAnnotation(bounds: CGRect(x: 10, y: 10, width: 100, height: 100), forType: PDFAnnotationSubtype.square, withProperties: nil)
        squareAnnotation.color = UIColor.blue
        
       
        let page = pdfDocument.page(at: 0)!
        page.addAnnotation(squareAnnotation)
        
  
//        (of: CGSize(
//                            width: pdfLabel.frame.size.width,
//                            height: pdfLabel.frame.size.height), for: .artBox)
        
        // Writing the changes to the file.
        pdfDocument.write(toFile: filePath)
        
//        if let path = Bundle.main.path(forResource: "MFI_2018_01", ofType: "pdf") {
//            let url = URL(fileURLWithPath: path)
//            if let pdfDocument = PDFDocument(url: url) {
//                pdfView.displayMode = .singlePageContinuous
//                pdfView.autoScales = true
//                // pdfView.displayDirection = .horizontal
//                pdfView.document = pdfDocument
//            }
//        }
    }
    
    
//    func captureThumbnails(pdfDocument:PDFDocument) {
//        if let page1 = pdfDocument.page(at: 1) {
//            page1ImageView.image = page1.thumbnail(of: CGSize(
//                width: page1ImageView.frame.size.width,
//                height: page1ImageView.frame.size.height), for: .artBox)
//        }
//
//        if let page2 = pdfDocument.page(at: 2) {
//            page2ImageView.image = page2.thumbnail(of: CGSize(
//                width: page2ImageView.frame.size.width,
//                height: page2ImageView.frame.size.height), for: .artBox)
//        }
//    }
}
